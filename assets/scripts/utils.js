$(document).ready(function () {

	var cnt = 0;
	var site = 'https://iotdashboard.mooo.com'
	//var site = 'https://ec2-34-250-126-107.eu-west-1.compute.amazonaws.com'
	
	function getData() {
	    return Math.random();
	}
	
	function getTemp(){
	  temp=""
	  $.ajax({
	    async: false,
	    url: site+'/getTemperatureStatistics',
	    type: 'get',
	    success: function(data) {
	      temp = data;
	    }
	  });
	  return temp
	}

    var resizeDebounce = null;

	function resizePlot() {
        var bb = temperaturePlotly.getBoundingClientRect();
        var nn = liquidPlotly.getBoundingClientRect();
        Plotly.relayout(temperaturePlotly, {
            width: bb.width,
            height: bb.height
        });
        Plotly.relayout(liquidPlotly, {
            width: nn.width,
            height: nn.height
        });
    }

	window.addEventListener('resize', function() {
		if (resizeDebounce) {
		    window.clearTimeout(resizeDebounce);
		}
		resizeDebounce = window.setTimeout(resizePlot, 100);
	});

	setInterval(function(){
		var val1 = getTemp()
		var val2 = getTemp()
		Plotly.extendTraces(temperaturePlotly,{ y:[[val1]]}, [0]);
		Plotly.extendTraces(temperaturePlotly,{ y:[[val2]]}, [1]);
		Plotly.extendTraces(liquidPlotly,{ y:[[val1]]}, [0]);
		
		//gauge.set(val1);
	  	//barHumidity.set(val1);
	  	//barTemperature.set(val2);
		cnt++;
		if(cnt > 500) {
			Plotly.relayout(temperaturePlotly,{
		    	xaxis: {
		        	range: [cnt-500,cnt]
		    	}
		    });
		    Plotly.relayout(liquidPlotly,{
		    	xaxis: {
		        	range: [cnt-500,cnt]
		    	}
		    });
		}
	},1000);

	//LAYOUT PLOTLY
	var WIDTH_IN_PERCENT_OF_PARENT = 100, HEIGHT_IN_PERCENT_OF_PARENT =30;
	//LAYOUT PLOTLY

	//PLOTLY TEMPERATURE

	var d3 = Plotly.d3;
	var gd3 = d3.select('.charthum')
	    .append('div')
	    .style({
	        width: WIDTH_IN_PERCENT_OF_PARENT + '%', 'margin-left': (100 - WIDTH_IN_PERCENT_OF_PARENT) / 2 + '%',

	        height: HEIGHT_IN_PERCENT_OF_PARENT + 'vh', 'margin-top': '0 vh'

	    });
	var temperaturePlotly = gd3.node();
	Plotly.plot(temperaturePlotly, {
                    data: [{"opacity": 1, "showlegend": false, "yaxis": "y2", "ysrc": "Alexxanddr:42:160fb1", "connectgaps": false, "mode": "lines", "y": ["0", "28", "30", "26", "29", "27", "30", "28", "29", "30", "30", "29", "31", "31", "27", "26", "26", "26", "31", "29", "31", "29", "31", "29", "30"], "line": {"dash": "solid", "color": "rgb(100, 114, 141)", "shape": "spline", "smoothing": 1.3, "width": 2}, "type": "scatter", "fill": "none"}, {"showlegend": false, "ysrc": "Alexxanddr:42:ecc6cc", "connectgaps": false, "mode": "lines", "y": ["0", "27", "28", "27", "29", "30", "29", "29", "29", "29", "28", "28", "28", "30", "30", "26", "26", "31", "26", "26", "30", "26", "26", "26", "30"], "line": {"dash": "solid", "color": "rgb(245, 166, 35)", "shape": "spline", "smoothing": 1.3, "width": 2}, "type": "scatter", "fill": "none"}],
                    layout: {"clickmode": "event", "autosize": false, "yaxis": {"showspikes": false, "automargin": true, "showticklabels": true, "tickformat": "", "tickmode": "auto", "separatethousands": false, "tickangle": "auto", "title": "Temperature (C\u00b0)", "side": "left", "range": [7.222222222222221, 62.77777777777778], "ticks": "", "zeroline": false, "nticks": 20, "type": "linear", "autorange": false, "borderwidth": 10}, "showlegend": true, "height": 207, "width": 730.5, "yaxis2": {"automargin": true, "title": "Relative Humidity (%)", "range": [10, 100], "showgrid": false, "overlaying": "y", "zeroline": false, "showline": false, "type": "linear", "autorange": false, "side": "right"}, "xaxis": {"showticklabels": true, "borderwidth": 10, "nticks": 23, "autorange": true, "showspikes": false, "separatethousands": false, "tickformat": "", "tickmode": "auto", "title": "Time (ss)", "ticks": "", "showgrid": true, "zeroline": false, "type": "linear", "rangeslider": {"visible": false, "range": [0, 60], "yaxis2": {}, "autorange": true, "yaxis": {}}, "automargin": true, "fixedrange": false, "showline": false, "tick0": 9, "tickangle": "auto", "gridwidth": 1, "range": [0, 24], "side": "bottom"}, "margin": {"pad": 1, "r": 20, "b": 20, "l": 20, "t": 20}}
                });

	//PLOTLY TEMPERATURE


	//PLOTLY LIQUID

	var d4 = Plotly.d3;
	var gd4 = d4.select('.chart-liquid')
	    .append('div')
	    .style({
	        width: WIDTH_IN_PERCENT_OF_PARENT + '%', 'margin-left': (100 - WIDTH_IN_PERCENT_OF_PARENT) / 2 + '%',

	        height: HEIGHT_IN_PERCENT_OF_PARENT + 'vh', 'margin-top':  '0 vh'

	    });
	var liquidPlotly = gd4.node();
	Plotly.plot(liquidPlotly, {
                    data: [{"opacity": 1, "showlegend": false, "ysrc": "Alexxanddr:44:cefa81", "connectgaps": false, "mode": "lines", "y": ["1", "20", "30", "20", "26", "30", "28", "29", "29", "30", "31", "28", "31", "27", "27", "28", "26", "30", "26", "30", "31", "30", "30", "28", "29", "29", "29", "29", "31", "30", "29", "27", "27", "30", "26", "29", "30", "27", "26", "30", "26", "30", "30", "26", "26", "28", "26", "28", "26", "28", "31", "31", "28", "29", "31", "30", "26", "30", "27", "30", "26", "26"], "line": {"dash": "solid", "width": 2, "shape": "spline", "smoothing": 1.3}, "type": "scatter", "fill": "none"}],
                    layout: {"clickmode": "event", "autosize": true, "yaxis": {"showspikes": false, "automargin": true, "showticklabels": true, "tickformat": "", "tickmode": "auto", "separatethousands": false, "tickangle": "auto", "title": "E (MJ)", "range": [-0.6666666666666663, 32.666666666666664], "ticks": "", "zeroline": false, "nticks": 20, "type": "linear", "autorange": true, "borderwidth": 10}, "height": 207, "width": 730.5, "xaxis": {"showspikes": false, "automargin": true, "showticklabels": false, "tickformat": "", "tickmode": "auto", "separatethousands": false, "tick0": 9, "tickangle": "auto", "ticks": "", "gridwidth": 1, "range": [0, 61], "borderwidth": 10, "zeroline": false, "nticks": 23, "autorange": true, "rangeslider": {"visible": false, "range": [0, 12], "autorange": true, "yaxis": {}}}, "margin": {"pad": 1, "r": 20, "b": 20, "l": 20, "t": 20}}
                });

	//PLOTLY LIQUID

	 var i = (HEIGHT_IN_PERCENT_OF_PARENT/2)-0.55
	$(".pan-indicator").css("width", "100%").css("margin-left","0%").css("height",i+"vh")
	$("#container-canvas").css("width", "100%").css("margin-left","0%").css("height",i+"vh")




	   var opts = {
	  angle: -0.16, // The span of the gauge arc
	  lineWidth: 0.05, // The line thickness
	  radiusScale: 0.84, // Relative radius
	  pointer: {
	    length: 0.19, // // Relative to gauge radius
	    strokeWidth: 0.033, // The thickness
	    color: '#000000' // Fill color
	  },
	  limitMax: false,     // If false, max value increases automatically if value > maxValue
	  limitMin: false,     // If true, the min value of the gauge will be fixed
	  colorStart: '#33AEC7',   // Colors
	  colorStop: '#64728D',    // just experiment with them
	  strokeColor: '#E0E0E0',  // to see which ones work best for you
	  generateGradient: true,
	  highDpiSupport: true,   
	    // High resolution support
	  
	};
	var target = document.getElementById('gaugeDemo'); // your canvas element
	target.style.transform = "rotate(290deg)";
	var gauge = new Gauge(target).setOptions(opts); // create sexy gauge!
	gauge.maxValue = 70; // set max gauge value
	gauge.setMinValue(0);  // Prefer setter over gauge.minValue = 0
	gauge.animationSpeed = 32; // set animation speed (32 is default value)
	gauge.set(12);
	 // set actual value


})
