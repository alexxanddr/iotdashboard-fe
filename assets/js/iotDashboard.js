$(document).ready(function () {

	var cnt = 0;
	var site = 'http://restpy-test.127.0.0.1.nissadfadsdfdsp.io'
	//var site = 'https://ec2-34-250-126-107.eu-west-1.compute.amazonaws.com'
	
	function getTemp(){
	  temp=""
	  $.ajax({
	    async: false,
	    url: site+'/getTemperature',
	    type: 'get',
	    success: function(data) {
	      temp = data;
	      resizePlot()
	    }
	  });
	  return temp
	}

	function getHumidity(){
          temp=""
          $.ajax({
            async: false,
            url: site+'/getHumidity',
            type: 'get',
            success: function(data) {
              temp = data;
              resizePlot()
            }
          });
          return temp
        }

	function getLiquid(){
          temp=""
          $.ajax({
            async: false,
            url: site+'/getLiquidLevel',
            type: 'get',
            success: function(data) {
              temp = data;
              resizePlot()
            }
          });
          return temp
        }

    var resizeDebounce = null;

	function resizePlot() {
        var bb = temperaturePlotly.getBoundingClientRect();
        var nn = energyPlotly.getBoundingClientRect();
        Plotly.relayout(temperaturePlotly, {
            width: bb.width,
            height: bb.height
        });
        Plotly.relayout(energyPlotly, {
            width: nn.width,
            height: nn.height
        });
    }

	window.addEventListener('resize', function() {
		if (resizeDebounce) {
		    window.clearTimeout(resizeDebounce);
		}
		resizeDebounce = window.setTimeout(resizePlot, 100);
	});

	setInterval(function(){

		var temperature = getTemp()
		var humidity = getHumidity()
		var energy = getLiquid()
		$('#tempDiv').text(temperature+' C�')
		$('#humDiv').text(humidity+' %')
		$('#enerDiv').text(energy+' Power (kW)')

		if (temperature > 30){
			if($( "#ring-alarm" ).hasClass( "disabled" )){
				$('#ring-alarm').toggleClass('disabled');
			}	
		}else if(temperature < 30){
			if(!$( "#ring-alarm" ).hasClass( "disabled" )){
				$('#ring-alarm').toggleClass('disabled');
			}	
		}

		if (humidity > 50){
			if($( "#ring-alarm-hum" ).hasClass( "disabled-hum" )){
				$('#ring-alarm-hum').toggleClass('disabled-hum');
			}	
		}else if(humidity < 50){
			if(!$( "#ring-alarm-hum" ).hasClass( "disabled-hum" )){
				$('#ring-alarm-hum').toggleClass('disabled-hum');
			}	
		}

		

		console.log('Temp - '+parseFloat(temperature).toFixed(2))
		console.log('Hum - '+humidity)
		
		Plotly.extendTraces(temperaturePlotly,{ y:[[temperature]]}, [1]);
		Plotly.extendTraces(temperaturePlotly,{ y:[[humidity]]}, [0]);
		Plotly.extendTraces(energyPlotly,{ y:[[energy]]}, [0]);

		temperatureGauge.set(temperature);
		humidityGauge.set(humidity);
		energyGauge.set(energy);

		cnt++;
		if(cnt > 500) {
			Plotly.relayout(temperaturePlotly,{
		    	xaxis: {
		        	range: [cnt-500,cnt]
		    	}
		    });
		    Plotly.relayout(energyPlotly,{
		    	xaxis: {
		        	range: [cnt-500,cnt]
		    	}
		    });
		}
	},3000);

	//LAYOUT PLOTLY
	var WIDTH_IN_PERCENT_OF_PARENT = 100, HEIGHT_IN_PERCENT_OF_PARENT =32;
	//LAYOUT PLOTLY

	//PLOTLY TEMPERATURE

	var d3 = Plotly.d3;
	var gd3 = d3.select('.charthum')
	    .append('div')
	    .style({
	        width: WIDTH_IN_PERCENT_OF_PARENT + '%', 'margin-left': (100 - WIDTH_IN_PERCENT_OF_PARENT) / 2 + '%',

	        height: HEIGHT_IN_PERCENT_OF_PARENT + 'vh', 'margin-top': '0 vh'

	    });
	var temperaturePlotly = gd3.node();
	Plotly.plot(temperaturePlotly, {
	data: [{
		"opacity": 1,
		"showlegend": false,
		"yaxis": "y2",
		"ysrc": "Alexxanddr:46:446541",
		"connectgaps": false,
		"mode": "lines",
		"y": [],
		"line": {
			"dash": "solid",
			"color": "rgb(245, 166, 35)",
			"shape": "spline",
			"smoothing": 1.3,
			"width": 2
		},
		"type": "scatter",
		"fill": "none"
	}, {
		"showlegend": false,
		"ysrc": "Alexxanddr:46:c6bf8f",
		"connectgaps": false,
		"mode": "lines",
		"y": [],
		"line": {
			"dash": "solid",
			"color": "rgb(100, 114, 141)",
			"shape": "spline",
			"smoothing": 1.3,
			"width": 2
		},
		"type": "scatter",
		"fill": "none"
	}],
	layout: {
		"clickmode": "event",
		"autosize": false,
		"yaxis": {
			"showspikes": false,
			"automargin": true,
			"showticklabels": true,
			"tickformat": "",
			"tickmode": "auto",
			"separatethousands": false,
			"tickangle": "auto",
			"title": "Temperature (C\u00b0)",
			"side": "left",
			"range": [7.222222222222221, 62.77777777777778],
			"ticks": "",
			"zeroline": false,
			"titlefont": {"family": "Avenir"},
			"nticks": 20,
			"type": "linear",
			"autorange": false,
			"borderwidth": 10
		},
		"showlegend": true,
		"height": 240,
		"width": 730.5,
		"yaxis2": {
			"automargin": true,
			"title": "Relative Humidity (%)",
			"titlefont": {"family": "Avenir"},
			"range": [10, 100],
			"showgrid": false,
			"overlaying": "y",
			"zeroline": false,
			"showline": false,
			"type": "linear",
			"autorange": false,
			"side": "right"
		},
		"xaxis": {
			"showticklabels": true,
			"borderwidth": 10,
			"nticks": 23,
			"autorange": true,
			"showspikes": false,
			"separatethousands": false,
			"tickformat": "",
			"tickmode": "auto",
			"title": "Time (s)",
			"ticks": "",
			"showgrid": true,
			"zeroline": false,
			"rangeslider": {
				"visible": false,
				"range": [0, 60],
				"yaxis2": {},
				"autorange": true,
				"yaxis": {}
			},
			"automargin": true,
			"fixedrange": false,
			"showline": false,
			"tick0": 9,
			"tickangle": "auto",
			"gridwidth": 1,
			"titlefont": {"family": "Avenir"},
			"side": "bottom",
			"range": [0, 9],
			"anchor": "y"
		},
		"margin": {
			"pad": 1,
			"r": 20,
			"b": 20,
			"l": 20,
			"t": 20
		}
	}
});

	//PLOTLY TEMPERATURE


	//PLOTLY ENERGY

	var d4 = Plotly.d3;
	var gd4 = d4.select('.chart-liquid')
	    .append('div')
	    .style({
	        width: WIDTH_IN_PERCENT_OF_PARENT + '%', 'margin-left': (100 - WIDTH_IN_PERCENT_OF_PARENT) / 2 + '%',

	        height: HEIGHT_IN_PERCENT_OF_PARENT + 'vh', 'margin-top':  '0 vh'

	    });
	var energyPlotly = gd4.node();
	Plotly.plot(energyPlotly, {
	data: [{
		"opacity": 1,
		"showlegend": false,
		"ysrc": "Alexxanddr:53:481d55",
		"connectgaps": false,
		"mode": "lines",
		"y": [99],
		"line": {
			"dash": "solid",
			"width": 2,
			"shape": "spline",
			"smoothing": 1.3
		},
		"type": "scatter",
		"fill": "none"
	}],
	layout: {
		"clickmode": "event",
		"autosize": true,
		"yaxis": {
			"showspikes": false,
			"automargin": true,
			"showticklabels": true,
			"tickformat": "",
			"tickmode": "auto",
			"separatethousands": false,
			"titlefont": {"family": "Avenir"},
			"tickangle": "auto",
			"title": "Power (kW)",
			"range": [-14.5, 275.5],
			"ticks": "",
			"zeroline": false,
			"nticks": 20,
			"type": "linear",
			"autorange": true,
			"borderwidth": 10
		},
		"height": 202.234375,
		"width": 753,
		"xaxis": {
			"showspikes": false,
			"automargin": true,
			"showticklabels": true,
			"tickformat": "",
			"tickmode": "auto",
			"separatethousands": false,
			"tick0": 9,
			"tickangle": "auto",
			"title": "Time (s)",
			"gridwidth": 1,
			"titlefont": {"family": "Avenir"},
			"range": [0, 4],
			"ticks": "",
			"zeroline": false,
			"nticks": 23,
			"rangeslider": {
				"visible": false,
				"range": [0, 12],
				"autorange": true,
				"yaxis": {}
			},
			"autorange": true,
			"borderwidth": 10
		},
		"margin": {
			"pad": 1,
			"r": 20,
			"b": 20,
			"l": 20,
			"t": 20
		}
	}
});



	//PLOTLY ENERGY

	//CONTAINER CANVAS

	 var i = (HEIGHT_IN_PERCENT_OF_PARENT/2)-0.55
	$(".pan-indicator").css("width", "100%").css("margin-left","0%").css("height",i+"vh")
	$("#container-canvas").css("width", "100%").css("margin-left","0%").css("height",i+"vh")
	$(".ccanvas").css("width", "100%").css("margin-left","0%").css("height",i+"vh")
	$(".ccanvas-ringbell").css("width", "100%").css("margin-left","-90%").css("height",i+"vh").css("position","absolute")

	//CONTAINER CANVAS


	//TEMPERATURE GAUGE

	var optsTemp = {
	  angle: -0.25, 
	  lineWidth: 0.03, 
	  radiusScale: 0.62,
	  pointer: {
	    length: 0.13, 
	    strokeWidth: 0.02, 
	    color: '#64728D'
	  },
	  limitMax: false,     
	  limitMin: false,     
	  colorStart: '#64728D',   
	  colorStop: '#64728D',    
	  strokeColor: '#E0E0E0',  
	  generateGradient: true,
	  highDpiSupport: true,     
	};

	var tempGaugeDom = document.getElementById('temperatureGauge'); 

	tempGaugeDom.style.transform = "rotate(290deg)";

	var temperatureGauge = new Gauge(tempGaugeDom).setOptions(optsTemp); 
	//ctx.fillRect(100, 0, 80, 20);
	temperatureGauge.maxValue = 50; 
	temperatureGauge.setMinValue(0);  
	temperatureGauge.animationSpeed = 32; 

	//TEMPERATURE GAUGE

	//HUMIDITY GAUGE

	var optsHum = {
	  angle: -0.25, 
	  lineWidth: 0.03, 
	  radiusScale: 0.62,
	  pointer: {
	    length: 0.13, 
	    strokeWidth: 0.02, 
	    color: '#F5A623'
	  },
	  limitMax: false,     
	  limitMin: false,     
	  colorStart: '#F5A623',   
	  colorStop: '#F5A623',    
	  strokeColor: '#E0E0E0',  
	  generateGradient: true,
	  highDpiSupport: true,     
	};

	var humGaugeDom = document.getElementById('humidityGauge'); 

	humGaugeDom.style.transform = "rotate(290deg)";

	var humidityGauge = new Gauge(humGaugeDom).setOptions(optsHum); 
	//ctx.fillRect(100, 0, 80, 20);
	humidityGauge.maxValue = 100; 
	humidityGauge.setMinValue(0);  
	humidityGauge.animationSpeed = 32; 

	//HUMIDITY GAUGE

	//ENERG GAUGE

	var optsEnerg = {
	  angle: -0.25, 
	  lineWidth: 0.03, 
	  radiusScale: 0.62,
	  pointer: {
	    length: 0.13, 
	    strokeWidth: 0.02, 
	    color: '#489FE6'
	  },
	  limitMax: false,     
	  limitMin: false,     
	  colorStart: '#489FE6',   
	  colorStop: '#489FE6',    
	  strokeColor: '#E0E0E0',  
	  generateGradient: true,
	  highDpiSupport: true,     
	};

	var energGaugeDom = document.getElementById('energyGauge'); 

	energGaugeDom.style.transform = "rotate(290deg)";

	var energyGauge = new Gauge(energGaugeDom).setOptions(optsEnerg); 
	//ctx.fillRect(100, 0, 80, 20);
	energyGauge.maxValue = 100; 
	energyGauge.setMinValue(0);  
	energyGauge.animationSpeed = 32; 

	//ENERG GAUGE

})
